# README #

### What is this repository for? ###

This repository contains the Caltech CS 11 coding judge, which automates code testing in the cloud for the Caltech CS department.

Version 1

### How do I get set up? ###

See admin guide

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Deployment instructions

### Contribution guidelines ###

This is a private repository, which is updated exclusively by Connor Crowley at the moment.

### Who do I talk to? ###

Connor Crowley ccrowley@caltech.edu
Donnie Pinkston donnie@cms.caltech.edu