#!/bin/bash

# Get IP address of container
IP="$(sudo lxc-ls --fancy | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}")"

# Clear out main directory of AWS container
ssh ubuntu@$IP "
rm -r *
"

# Move AWS container ZIP file from server to container
scp cont_env.zip ubuntu@$IP:~/

# unzip and remove AWS container ZIP file on AWS container
ssh ubuntu@$IP "
unzip cont_env.zip
rm cont_env.zip
"

# remove AWS contianer ZIP file from AWS server
rm cont_env.zip