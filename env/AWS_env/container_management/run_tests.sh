#!/bin/bash

IP="$(sudo lxc-ls --fancy | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}")"

scp AWS_env/judge/cs11cpp_sp2016-lab1-20160928-133002.zip ubuntu@$IP:~/cont_env/

ssh ubuntu@$IP "
cd cont_env
ls
python3 ./test_cpp.py
exit
"

scp ubuntu@$IP:~/cont_env/errors.json /home/ubuntu/AWS_env/judge
