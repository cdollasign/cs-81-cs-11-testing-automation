#include "testbase.hpp"
#include "fibonacci.hpp"
#include <iostream>
#include <vector>
#include <unistd.h>
#include <csignal>
#include <err.h>
#include <cstdlib>
#include <cstring>
#include <sys/time.h>
#include <sys/resource.h>

using namespace std;

/*===========================================================================
 * TEST FUNCTIONS
 *
 * These are called by the main() function at the end of this file.
 */

/*! Test recursive fibonacci algorithm. */
void test_recursive(TestContext &ctx) {
    ctx.DESC("Recursive fibonacci algorithm");

    int zeroth = fib_rec(0);
    int first = fib_rec(1);
    int second = fib_rec(2);
    int third = fib_rec(3);
    int fourth = fib_rec(4);
    int fifth = fib_rec(5);

    ctx.CHECK(zeroth == 0);
    ctx.CHECK(first == 1);
    ctx.CHECK(second == 1);
    ctx.CHECK(third == 2);
    ctx.CHECK(fourth == 3);
    ctx.CHECK(fifth == 5);

    ctx.result();
}

/*! Test recursive fibonacci algorithm and have it timeout. */
void test_timeout(TestContext &ctx) {
    ctx.DESC("Recursive fibonacci algorithm");

    int zeroth = fib_rec(0);
    int first = fib_rec(1);
    int second = fib_rec(2);
    int third = fib_rec(3);
    int fourth = fib_rec(4);
    int fifth = fib_rec(5);
    int sixth = fib_rec(45);
    sixth += 1;

    ctx.CHECK(zeroth == 0);
    ctx.CHECK(first == 1);
    ctx.CHECK(second == 1);
    ctx.CHECK(third == 2);
    ctx.CHECK(fourth == 3);
    ctx.CHECK(fifth == 5);

    ctx.result();
}

/*! Test dynamic programming fibonacci algorithm. */
void test_dynamic_programming(TestContext &ctx) {
    ctx.DESC("Dynamic programming fibonacci algorithm");

    int zeroth = fib_dp(0);
    int first = fib_dp(1);
    int second = fib_dp(2);
    int third = fib_dp(3);
    int fourth = fib_dp(4);
    int fifth = fib_dp(5);

    ctx.CHECK(zeroth == 0);
    ctx.CHECK(first == 1);
    ctx.CHECK(second == 1);
    ctx.CHECK(third == 2);
    ctx.CHECK(fourth == 3);
    ctx.CHECK(fifth == 5);

    ctx.result();
}

/*! Test memoization fibonacci algorithm. */
void test_memoization(TestContext &ctx) {
    ctx.DESC("Memoized fibonacci algorithm");

    int zeroth = fib_memo(0);
    int first = fib_memo(1);
    int second = fib_memo(2);
    int third = fib_memo(3);
    int fourth = fib_memo(4);
    int fifth = fib_memo(5);

    ctx.CHECK(zeroth == 0);
    ctx.CHECK(first == 1);
    ctx.CHECK(second == 1);
    ctx.CHECK(third == 2);
    ctx.CHECK(fourth == 3);
    ctx.CHECK(fifth == 5);

    ctx.result();
}

/*! Test most efficient fibonacci algorithm. */
void test_most_efficient(TestContext &ctx) {
    ctx.DESC("Most efficient fibonacci algorithm");

    int zeroth = fib_cslt(0);
    int first = fib_cslt(1);
    int second = fib_cslt(2);
    int third = fib_cslt(3);
    int fourth = fib_cslt(4);
    int fifth = fib_cslt(5);

    ctx.CHECK(zeroth == 0);
    ctx.CHECK(first == 1);
    ctx.CHECK(second == 1);
    ctx.CHECK(third == 2);
    ctx.CHECK(fourth == 3);
    ctx.CHECK(fifth == 5);

    ctx.result();
}

/*! This program is a simple test-suite for the fibonacci program. */
int main(int argc, char *argv[]) {
    cout << "Testing fibonacci program." << endl << endl;

    TestContext ctx(cout);

    test_recursive(ctx);
    test_dynamic_programming(ctx);
    test_memoization(ctx);
    test_most_efficient(ctx);
    test_timeout(ctx);
    
    // Return 0 if everything passed, nonzero if something failed.
    return !ctx.ok();
}
