#!/bin/bash

SERVER_NAME=ubuntu@52.90.116.9
SSH_KEY=~/.ssh/MyFirstKey.pem

# check that user specified zip file to move to judge
if [ $# -ne 1 ]
    then
        echo "Wrong number of arguments supplied"
        echo "Should supply one argument"
else
    scp -i $SSH_KEY "$1" $SERVER_NAME:~
    ssh -i $SSH_KEY $SERVER_NAME "
    mv $1 judge
    cd judge
    python3 test.py
    rm $1
    "
    scp -i $SSH_KEY $SERVER_NAME:~/judge/errors.json .
    ssh -i $SSH_KEY $SERVER_NAME "
    cd judge
    rm errors.json
    "
fi