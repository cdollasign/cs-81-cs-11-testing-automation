#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include "Point.hh"

using namespace std;

double circuitLength(const vector<Point> &points, const vector<int> &order) {
	double total = 0;

	total += points[order[0]].distanceTo(points[order[order.size() - 1]]);

	for (unsigned int i = 1; i < order.size(); ++i) {
		double dist = points[order[i - 1]].distanceTo(points[order[i]]);
		total += dist;
	}

	return total;
}

double findShortestPath(const vector<Point> &points) {
	vector<int> order;

	for (unsigned int i = 0; i < points.size(); ++i) {
		order.push_back(i);
	}

	double shortest_dist = circuitLength(points, order);

	while (next_permutation(order.begin(), order.end())) {
		double dist = circuitLength(points, order);
		if (dist < shortest_dist) shortest_dist = dist;
	}

	return shortest_dist;
}

int main() {
	ifstream input("input.txt");
	ofstream output("result.txt");
	int num_cases, lines_per_case;
	input >> num_cases;
	for (int i = 0; i < num_cases; ++i) {
		vector<Point> points;
		input >> lines_per_case;
		for (int j = 0; j < lines_per_case; ++j) {
			int x, y, z;
			input >> x;
			input >> y;
			input >> z;
			Point p(x, y, z);
			points.push_back(p);
		}
		output << findShortestPath(points) << endl;
	}

	return 0;
}