#include "tsp-ga.hh"
#include <algorithm>
#include <vector>
#include <cfloat>

tsp-ga::TSPGenome(int numPoints) {
	vector<int> order;
	for (int i = 0; i < numPoints; ++i) {
		order.push_back(i);
	}
	std::random_shuffle(order.begin(), order.end());
	this.order = order;
	this.circuitLength = DBL_MAX;
}

tsp-ga::TSPGenome(const vector<int> &order) {
	this.order = order;
	this.circuitLength = DBL_MAX;
}

vector<int> tsp-ga::getOrder() const {
	return this.order;
}

void tsp-ga::computeCircuitLength(const vector<Point> points) {
	double total = 0;
	vector<int> order = this.order;

	total += points[order[0]].distanceTo(points[order[order.size() - 1]]);

	for (unsigned int i = 1; i < order.size(); ++i) {
		double dist = points[order[i - 1]].distanceTo(points[order[i]]);
		total += dist;
	}

	this.circuitLength = total;
}

double tsp-ga::getCircuitLength() const {
	return this.circuitLength;
}