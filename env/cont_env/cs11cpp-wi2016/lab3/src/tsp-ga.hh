#include <vector>

class TSPGenome {

private:
  vector<int> order;
  double circuitLength;

public:
  TSPGenome(int numPoints);
  TSPGenome(const vector<int> &order);
  vector<int> getOrder() const;
  void computeCircuitLength(const vector<int> points);
  double getCircuitLength() const;
};
