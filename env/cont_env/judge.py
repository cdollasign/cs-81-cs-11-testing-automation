from enum import Enum
import os
import zipfile
import tempfile
import shutil
import subprocess
import shlex
import json
import settings as s

class JudgeError(Enum):
    compilation = 1
    seg_fault = 2
    time_limit = 3
    memory_limit = 4
    wrong_answer = 5
    correct_answer = 6
    invalid_submission = 7

class Language(Enum):
    c = 1
    cpp = 2
    python = 3

class Judge:
    '''
    API to be used by csman for grading student assignments for the C and 
    C++ tracks of CS 11.
    '''
    def __init__(self):
        self.json_arr = {}

    def grade_total_submission(self, in_file, grading_style, language):
        '''
        Grade zipfile of multiple student submissions from csman using a
        specific grading style.  Return list of (grade, error) for
        submissions.
        '''
        result = []
        to_grade = self.unpack_submissions(in_file)
        for (zip_file, student) in to_grade:
            temp = self.grade_program(zip_file, grading_style, language, student)
            result.append(temp)

        self._output_json_to_file()

        return result

    def unpack_submissions(self, in_file):
        self._unpack_in_dir(in_file, s.JUDGE_ROOT)
        submissions = os.listdir('./lab1')
        result = []

        for i in range(len(submissions)):
            student = submissions[i].split('-')[0]
            zip_name = shutil.make_archive("./cs11cpp-wi2016_lab1_" + str(i),
                "zip", "./lab1/" + submissions[i])
            result.append((zip_name, student))

        return result

    def grade_program(self, in_file, grading_style, language, student):
        '''
        Grade compressed file submitted on csman using a specific grading
        style.  Return (grade, error) for a submission.
        '''
        json_dict = {s.VALID_INPUT_CHECK: "", s.VALID_FILES: "", \
            s.COMPILE_STDOUT: "", s.COMPILE_STDERR: "", \
            s.RUN_TEST_STDOUT: "", s.RUN_TEST_STDERR: "", s.GRADE: "0"}

        # Make sure the zip file is valid
        result = self._check_valid_in_file(in_file, json_dict)
        if result[0] == False:
            self._output_json(student, json_dict)
            return (0, JudgeError.invalid_submission)

        lab_dir = result[1]
        class_dir = result[2]
        temp_dir = self._create_temp_dir()
        code_bundle_dir = temp_dir + "/code_bundle"

        # Move all student submissions in zip file and testing code to tmp
        # directory for testing
        result = self._prepare_to_test(in_file, temp_dir, lab_dir, class_dir, \
            json_dict, language)

        # Return if zip file is false
        if result[0] == False:
            self._output_json(student, json_dict)
            return (0, JudgeError.invalid_submission)

        mem_lim = result[1]
        time_lim = result[2]

        # Compile code for testing
        if language != Language.python:
            result = self._build_test(code_bundle_dir, json_dict)

            # Return if student submission does not compile properly
            if result == False:
                self._output_json(student, json_dict)
                return (0, JudgeError.compilation)

        # Run tests on student code
        result = self._run_test(code_bundle_dir, mem_lim, time_lim, json_dict, language)

        # Return if tests, memory, or time limits fail for any reason
        if result[0] == False:
            self._output_json(student, json_dict)
            return (0, result[1])

        # Calculate the grade on the student submission
        result = self._make_grade(result[1])
        json_dict[s.GRADE] = str(result)

        # Output all errors as JSON
        self._output_json(student, json_dict)

        self._destroy_temp_dir(temp_dir)

        return (result, JudgeError.correct_answer)

    def _output_json(self, student, json_dict):
        self.json_arr[student] = json_dict

    def _output_json_to_file(self):
        with open(s.JSON_OUTPUT_DIR, 'w') as outfile:
            json.dump(self.json_arr, outfile)

    def _prepare_to_test(self, in_file, temp_dir, lab_dir, class_dir, json_dict, \
        language):
        '''
        Move all student submissions in zip file and testing code to tmp
        directory for testing
        '''
        self._unpack_in_dir(in_file, temp_dir)

        correct_files = self._check_correct_files(temp_dir, lab_dir, json_dict)

        if correct_files[0] == False:
            return (False, 0, 0)

        lab_zip = [f for f in os.listdir(lab_dir) \
            if os.path.isfile(os.path.join(lab_dir, f)) and f.endswith(".zip")]

        lab_zip = lab_zip[0]
        zip_location = lab_dir + "/" + lab_zip
        zip_dest = temp_dir + "/" + lab_zip
        shutil.copyfile(zip_location, zip_dest)

        with zipfile.ZipFile(zip_dest, "r") as z:
            z.extractall(temp_dir)

        dirs_in_temp = [f for f in os.listdir(temp_dir) \
            if os.path.isdir(os.path.join(temp_dir, f))]
        student_dir = [f for f in os.listdir(temp_dir) \
            if os.path.isfile(os.path.join(temp_dir, f))]

        for f in student_dir:
            src = temp_dir + "/" + f
            dst = temp_dir + "/code_bundle/src/" + f
            shutil.copyfile(src, dst)

        return correct_files

    def _check_valid_in_file(self, in_file, json_dict):
        '''
        Take a compressed in_file and return true or false based on whether or
        not the file is valid, along with the lab and class directories.
        '''
        info = in_file.split('/')
        class_info = info[4].split('_')
        class_dir = "/" + info[1] + "/" + info[2] + "/" + info[3] + "/" + class_info[0]
        lab_dir = class_dir + "/" + class_info[1]

        subdirectories = [x[0] for x in os.walk(s.JUDGE_ROOT)]

        if lab_dir in subdirectories:
            json_dict[s.VALID_INPUT_CHECK] = "Input is valid."
            return (True, lab_dir, class_dir)
        json_dict[s.VALID_INPUT_CHECK] = "Lab directory: " + lab_dir + " was "
        json_dict[s.VALID_INPUT_CHECK] += "not located in the file system."
        return (False, lab_dir, class_dir)

    def _unpack_in_dir(self, in_file, temp_dir):
        with zipfile.ZipFile(in_file, "r") as z:
            z.extractall(temp_dir)

    def _create_temp_dir(self):
        return tempfile.mkdtemp()

    def _destroy_temp_dir(self, temp_dir):
        shutil.rmtree(temp_dir)

    def _check_correct_files(self, temp_dir, lab_dir, json_dict):
        '''
        Check to make sure that the files included in the zip file match the
        ones required in the config file provided in the lab directory.
        '''
        in_files = [f for f in os.listdir(temp_dir)]
        config_dir = lab_dir + "/required.cfg"
        with open(config_dir) as f:
            content = f.readlines()
        config_dir = lab_dir + "/params.cfg"
        with open(config_dir) as f:
            params = f.readlines()
        for i in range(len(content)):
            content[i] = content[i].replace('\n', '')
        for i in range(len(params)):
            params[i] = params[i].replace('\n', '')
        params.sort()

        ml = params[0].split(' ')[1]
        tl = params[1].split(' ')[1]

        for c in content:
            if c not in in_files:
                json_dict[s.VALID_FILES] = "Provided files: " + str(in_files)
                json_dict[s.VALID_FILES] += " do not match required files: "
                json_dict[s.VALID_FILES] += str(content)
                return (False, 0, 0)

        json_dict[s.VALID_FILES] = "Input files are correct."
        return (True, ml, tl)

    def _build_test(self, code_bundle_dir, json_dict):
        '''
        Given the jail directory, build the testing suite, returning an error
        if compilation fails.
        '''
        os.chdir(code_bundle_dir)
        make_process = subprocess.Popen("make", stdout = subprocess.PIPE, \
            stderr = subprocess.PIPE)
        out = make_process.communicate()
        stdout = out[0]
        stderr = out[1]
        json_dict[s.COMPILE_STDOUT] = str(stdout)
        json_dict[s.COMPILE_STDERR] = str(stderr)
        if make_process.wait() != 0:
            return False
        return True

    def _run_test(self, code_bundle_dir, mem_lim, time_lim, json_dict, language):
        '''
        Run the test suite in the given jail directory, returning any errors
        resulting from running the tests.
        '''
        os.chdir(code_bundle_dir)
        if language != Language.python:
            abs_dir = os.path.abspath("./bin_secure/")
            fib_test_dir = "./lab1"
            if language == Language.c: language = "c"
            elif language == Language.cpp: language = "cpp"
            run_string = s.LIMIT_ENFORCER_DIR + " -m " + str(mem_lim) + \
                " -t " + str(time_lim) + " -p " + fib_test_dir + " -d " + abs_dir \
                + " -l " + language
        else:
            abs_dir = os.path.abspath("./src")
            fib_test_dir = "./fib_test.py"
            if language == Language.python: language = "python"
            run_string = s.LIMIT_ENFORCER_DIR + " -m " + str(mem_lim) + \
                " -t " + str(time_lim) + " -p " + fib_test_dir + " -d " + abs_dir \
                + " -l " + language
        args = shlex.split(run_string)
        my_input = open("./bin_secure/input.txt")
        make_process = subprocess.Popen(args, stdin = my_input, \
            stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        out = make_process.communicate()
        stdout = out[0]
        stderr = out[1]

        json_dict[s.RUN_TEST_STDOUT] = str(stdout)
        json_dict[s.RUN_TEST_STDERR] = str(stderr)
        if make_process.wait() != 0:
            return (False, JudgeError.seg_fault)

        if b"TLE" in stderr:
            return (False, JudgeError.time_limit)
        elif b"MLE" in stderr:
            return (False, JudgeError.memory_limit)
        
        os.chdir("./grading")
        os.system("python3 /home/ubuntu/cont_env/grading.py")
        with open("passed.txt", "r") as p, open("num_cases.txt", "r") as n:
            passed = int(p.readline())
            total = int(n.readline())
        os.chdir(code_bundle_dir)

        return (True, (passed, total))

    def _make_grade(self, grade):
        '''
        Given the grading style and the error variable, return the
        appropriate grade for the student submission.
        '''
        return grade
