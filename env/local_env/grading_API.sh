#!/bin/bash

SERVER_NAME=$2
SSH_KEY=$3

# check that user specified zip file to move to judge
if [ $# -ne 3 ]
    then
        echo "Wrong number of arguments supplied"
        echo "Should supply one argument"
else
    scp -i $SSH_KEY "$1" $SERVER_NAME:~/AWS_env/judge/

    ssh -i $SSH_KEY $SERVER_NAME "
    sh ./AWS_env/container_management/run_tests.sh $1
    cd ./AWS_env/judge
    rm $1
    "
    scp -i $SSH_KEY $SERVER_NAME:~/AWS_env/judge/errors.json .
    ssh -i $SSH_KEY $SERVER_NAME "
    cd ./AWS_env/judge
    rm errors.json
    "
fi
