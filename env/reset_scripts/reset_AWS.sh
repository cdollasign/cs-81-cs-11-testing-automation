#!/bin/bash

SERVER_NAME=$1
SSH_KEY=$2

# zip AWS environment for scp
zip -r AWS_env.zip AWS_env

# clear out AWS main directory
ssh -i $SSH_KEY $SERVER_NAME "
rm -r *
"

# copy over AWS environment zip file
scp -i $SSH_KEY AWS_env.zip $SERVER_NAME:~/

# unzip AWS environment on AWS server
ssh -i $SSH_KEY $SERVER_NAME "
unzip AWS_env.zip
rm AWS_env.zip
"

# remove AWS environment zip file from local machine
rm AWS_env.zip
