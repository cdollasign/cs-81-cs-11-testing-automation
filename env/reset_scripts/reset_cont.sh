#!/bin/bash

SERVER_NAME=$1
SSH_KEY=$2

# zip AWS container environment for scp
zip -r cont_env.zip cont_env

# copy over AWS container environment zip file
scp -i $SSH_KEY cont_env.zip $SERVER_NAME:~/AWS_env/container_management

# reset AWS container from AWS server
ssh -i $SSH_KEY $SERVER_NAME "
cd /home/ubuntu/AWS_env/container_management/
sh reset_cont.sh
"

# remove AWS container environment zip file from local machine
rm cont_env.zip
