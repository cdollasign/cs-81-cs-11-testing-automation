#!/bin/bash

SERVER_NAME=ubuntu@54.144.192.13
SSH_KEY=~/.ssh/new_server.pem
DIRECTORY=/Users/connor/dev/cs-81-cs-11-testing-automation/env

cd $DIRECTORY

# Reset AWS server
sh reset_scripts/reset_AWS.sh $SERVER_NAME $SSH_KEY

# Reset AWS container on AWS server
sh reset_scripts/reset_cont.sh $SERVER_NAME $SSH_KEY

cd local_env/

# Run C++ test
sh grading_API.sh cs11cpp_sp2016-lab1-20160928-133002.zip $SERVER_NAME $SSH_KEY
