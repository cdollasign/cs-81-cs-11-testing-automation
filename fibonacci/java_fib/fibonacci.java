import java.util.Scanner;
 
class Fibonacci
{
   public static void main(String args[])
   {
      System.out.println("recursive n = 25");
      System.out.println(fib_rec(25));
      System.out.println("efficient n = 25");
      System.out.println(fib_cslt(25));
   }

   public static int fib_rec(int n) {
      if (n == 0) return 0;
      if (n == 1 || n == 2) return 1;
      return fib_rec(n - 1) + fib_rec(n - 2);
   }

   public static int fib_cslt(int n) {
      if (n == 0) return 0;
      if (n == 1) return 1;

      int prev = 0;
      int curr = 1;

      for (int i = 2; i < n + 1; ++i) {
         int nxt = prev + curr;
         prev = curr;
         curr = nxt;
      }

      return curr;
   }
}