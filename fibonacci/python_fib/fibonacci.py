''' Recursive implementation of fibonacci.  Input a number n >= 0, and the
function will return the nth fibonacci number. This function runs in
exponential time and linear space. '''
def fib_rec(n):
	if (n == 0): return 0
	if (n == 1 or n == 2): return 1
	return fib_rec(n - 1) + fib_rec(n - 2)

''' Dynamic programming implementation of fibonacci.  Input a number n >= 0, and
the function will return the nth fibonacci number.  This function runs in
linear time and linear space. '''
def fib_dp(n):
	dp_table = [0, 1]

	for i in range(2, n + 1):
		nxt = dp_table[i - 1] + dp_table[i - 2]
		dp_table.append(nxt)

	return dp_table[n]

''' Most efficient implementation of fibonacci.  Input a number n >= 0, and the
function will return the nth fibonacci number.  This function runs in linear
time and constant space. '''
def fib_cslt(n):
	if (n == 0): return 0
	if (n == 1): return 1

	prev = 0
	curr = 1

	for i in range(2, n + 1):
		nxt = prev + curr
		prev = curr
		curr = nxt

	return curr