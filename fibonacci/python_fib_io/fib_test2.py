from fibonacci import *

with open("input.txt", "r") as inp, open("result.txt", "w") as out:
    num_cases = int(inp.readline())
    lines_per_case = int(inp.readline())
    print ("num_cases = " + str(num_cases))
    print ("lines_per_case = " + str(lines_per_case))
    for line in inp:
        num = int(line)
        print(num)
        result = fib_cslt(num)
        out.write("%d\n" % result)