def check_test(ans, stud):
    if (len(ans) != len(stud)): return False
    for i in range(len(ans)):
        ans[i] = ans[i].replace('\n', '')
        stud[i] = stud[i].replace('\n', '')
        if ans[i] != stud[i]:
            return False
    return True

with open("output.txt", "r") as answers, open("result.txt", "r") as student:
    num_cases = int(answers.readline())
    lines_per_case = int(answers.readline())
    results = []
    for i in range(num_cases):
        ans = []
        stud = []
        for j in range(lines_per_case):
            ans.append(answers.readline())
            stud.append(student.readline())
        res = check_test(ans, stud)
        results.append((res, ans, stud))
    passed = 0
    for i in range(len(results)):
        if results[i][0] == True:
            passed += 1
        else:
            print (" ")
            print ("Failed test case " + str(i + 1))
            print ("got:")
            print (results[i][2])
            print ("expected:")
            print (results[i][1])
            print (" ")

    print ("grade = " + str(passed) + " out of " + str(num_cases))