#simple GUI

from Tkinter import *

# # create the window
# root = Tk()

# # modify root window
# root.title("Caltech CS Autograder")
# root.geometry("1000x500")

# # kick off the event loop
# root.mainloop()

master = Tk()

# # modify root window
master.title("Caltech CS Autograder")
master.geometry("1000x500")

e = Entry(master)
e.pack(side = LEFT)

e.focus_set()

def callback():
    print e.get()

b = Button(master, text="get", width=10, command=callback)
b.pack(side = LEFT)

mainloop()

e = Entry(master, width=50)
e.pack(side = LEFT)

text = e.get()

def makeentry(parent, caption, width=None, **options):
    Label(parent, text=caption).pack(side=LEFT)
    entry = Entry(parent, **options)
    if width:
        entry.config(width=width)
    entry.pack(side=LEFT)
    return entry

user = makeentry(parent, "User name:", 10)
password = makeentry(parent, "Password:", 10, show="*")

content = StringVar()
entry = Entry(parent, text=caption, textvariable=content)

text = content.get()
content.set(text)