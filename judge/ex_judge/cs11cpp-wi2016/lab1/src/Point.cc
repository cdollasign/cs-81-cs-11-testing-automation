#include <cmath>
#include "Point.hh"


// Default constructor:  initializes the point to (0, 0, 0).
Point::Point() {
  x_coord = 0;
  y_coord = 0;
  z_coord = 0;
}

// Initializes the point to (x, y, z).
Point::Point(double x, double y, double z) {
  x_coord = x;
  y_coord = y;
  z_coord = z;
}

// Destructor - Point allocates no dynamic resources.
Point::~Point() {
  // no-op
}

// Mutators:

void Point::setX(double val) {
  x_coord = val;
}

void Point::setY(double val) {
  y_coord = val;
}

void Point::setZ(double val) {
	z_coord = val;
}

// Accessors:

double Point::getX() {
  return x_coord;
}

double Point::getY() {
  return y_coord;
}

double Point::getZ() {
	return z_coord;
}

double Point::distanceTo(Point &p) {
	double x_dist = (this->x_coord - p.x_coord) * (this->x_coord - p.x_coord);
	double y_dist = (this->y_coord - p.y_coord) * (this->y_coord - p.y_coord);
	double z_dist = (this->z_coord - p.z_coord) * (this->z_coord - p.z_coord);
	return sqrt(x_dist + y_dist + z_dist);
}