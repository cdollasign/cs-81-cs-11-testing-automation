from fibonacci import *

'''===========================================================================
 * TEST FUNCTIONS
 *
 * These are called by the main() function at the end of this file.
'''

'''! Test recursive fibonacci algorithm. '''
def test_recursive():
    print("Recursive fibonacci algorithm")

    zeroth = fib_rec(0)
    first = fib_rec(1)
    second = fib_rec(2)
    third = fib_rec(3)
    fourth = fib_rec(4)
    fifth = fib_rec(5)
    sixth = fib_rec(35)

    if zeroth != 0: print("t1 failed")
    if first != 1: print("t2 failed")
    if second != 1: print("t3 failed")
    if third != 2: print("t4 failed")
    if fourth != 3: print("t5 failed")
    if fifth != 5: print("t6 failed")
    if sixth != 9227465: print("t7 failed")

'''! Test dynamic programming fibonacci algorithm. '''
def test_dynamic_programming():
    print("Dynamic programming fibonacci algorithm")

    zeroth = fib_dp(0)
    first = fib_dp(1)
    second = fib_dp(2)
    third = fib_dp(3)
    fourth = fib_dp(4)
    fifth = fib_dp(5)
    sixth = fib_dp(35)

    if zeroth != 0: print("t1 failed")
    if first != 1: print("t2 failed")
    if second != 1: print("t3 failed")
    if third != 2: print("t4 failed")
    if fourth != 3: print("t5 failed")
    if fifth != 5: print("t6 failed")
    if sixth != 9227465: print("t7 failed")    

'''! Test most efficient fibonacci algorithm. '''
def test_most_efficient():
    print("Most efficient fibonacci algorithm")

    zeroth = fib_cslt(0)
    first = fib_cslt(1)
    second = fib_cslt(2)
    third = fib_cslt(3)
    fourth = fib_cslt(4)
    fifth = fib_cslt(5)
    sixth = fib_cslt(35)

    if zeroth != 0: print("t1 failed")
    if first != 1: print("t2 failed")
    if second != 1: print("t3 failed")
    if third != 2: print("t4 failed")
    if fourth != 3: print("t5 failed")
    if fifth != 5: print("t6 failed")
    if sixth != 9227465: print("t7 failed")

'''! This program is a simple test-suite for the fibonacci program. '''
print ("Testing fibonacci program.\n\n")

test_recursive()
test_dynamic_programming()
test_most_efficient()