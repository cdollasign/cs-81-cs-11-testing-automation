#include "Point.hh"
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>

using namespace std;

double computeArea(Point &a, Point &b, Point &c) {
	double ab = a.distanceTo(b);
	double bc = b.distanceTo(c);
	double ca = c.distanceTo(a);

	double s = (ab + bc + ca) / 2;
	return sqrt(s * (s - ab) * (s - bc) * (s - ca));
}

int main() {
	ifstream input("input.txt");
	ofstream output("result.txt");
	int num_cases, lines_per_case;
	input >> num_cases;
	input >> lines_per_case;
	for (int i = 0; i < num_cases; ++i) {
		int x, y, z;
		input >> x;
		input >> y;
		input >> z;
		Point a(x, y, z);
		input >> x;
		input >> y;
		input >> z;
		Point b(x, y, z);
		input >> x;
		input >> y;
		input >> z;
		Point c(x, y, z);
		output << computeArea(a, b, c) << endl;
	}

	return 0;
}