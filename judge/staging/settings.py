import os

JUDGE_ROOT = os.getcwd()
JSON_OUTPUT_DIR = JUDGE_ROOT + "/errors.json"
LIMIT_ENFORCER_DIR = "/usr/bin/limit_enforcer"

# JSON output categories
VALID_INPUT_CHECK = "valid_input_check"
VALID_FILES = "valid_files"
COMPILE_STDOUT = "compile_stdout"
COMPILE_STDERR = "compile_stderr"
RUN_TEST_STDOUT = "run_test_stdout"
RUN_TEST_STDERR = "run_test_stderr"
GRADE = "grade"