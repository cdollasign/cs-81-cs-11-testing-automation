#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

/* Recursive implementation of fibonacci.  Input a number n >= 0, and the
function will return the nth fibonacci number. This function runs in
exponential time and linear space. */
int fib_rec(int n) {
	if (n == 0) return 0;
	if (n == 1 || n == 2) return 1;
	return fib_rec(n - 1) + fib_rec(n - 2);
}

/* Dynamic programming implementation of fibonacci.  Input a number n >= 0, and
the function will return the nth fibonacci number.  This function runs in
linear time and linear space. */
int fib_dp(int n) {
	vector<int> dp_table;
	dp_table.push_back(0);
	dp_table.push_back(1);

	for (int i = 2; i <= n; ++i) {
		int next = dp_table[i - 1] + dp_table[i - 2];
		dp_table.push_back(next);
	}

	return dp_table[n];
}

/* Recursive utility function that helps run the memoized fibonacci function.
*/
int fib_memo_util(int n, unordered_map<int, int> &m) {
	unordered_map<int, int>::const_iterator it1 = m.find(n - 1);
	unordered_map<int, int>::const_iterator it2 = m.find(n - 2);
	int first, second;

	if (it1 == m.end()) {
		first = fib_memo_util(n - 1, m);
		m[n - 1] = first;
	}
	else first = m[n - 1];

	if (it2 == m.end()) {
		second = fib_memo_util(n - 2, m);
		m[n - 2] = second;
	}
	else second = m[n - 2];

	return first + second;
}

/* Memoization implementation of fibonacci.  Input a number n >= 0, and
the function will return the nth fibonacci number.  This function runs in
linear time and linear space. */
int fib_memo(int n) {
	unordered_map<int, int> m;
	if (n == 0) return 0;
	if (n == 1) return 1;
	m[0] = 0;
	m[1] = 1;
	return fib_memo_util(n, m);
}

/* Most efficient implementation of fibonacci.  Input a number n >= 0, and the
function will return the nth fibonacci number.  This function runs in linear
time and constant space. */
int fib_cslt(int n) {
	if (n == 0) return 0;
	if (n == 1) return 1;

	int prev = 0;
	int curr = 1;

	for (int i = 2; i <= n; ++i) {
		int next = prev + curr;
		prev = curr;
		curr = next;
	}

	return curr;
}