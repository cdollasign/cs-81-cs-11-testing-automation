#ifndef FIBONACCI_HPP
#define FIBONACCI_HPP

#include <unordered_map>

using namespace std;

int fib_rec(int n);

int fib_dp(int n);

int fib_memo_util(int n, unordered_map<int, int> &m);

int fib_memo(int n);

int fib_cslt(int n);

#endif // FIBONACCI_HPP