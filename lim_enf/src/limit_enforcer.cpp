#include "testbase.hpp"
#include "fibonacci.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <unistd.h>
#include <csignal>
#include <err.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <sys/stat.h>
#include <getopt.h>
#include <fcntl.h>

/* Vivek Prakash's Quora posts were very helpful in designing this document.
https://www.quora.com/How-do-online-judges-identify-Time-Limit-Exceeded
https://www.quora.com/What-is-the-simplest-and-most-accurate-way-to-measure-
    the-memory-used-by-a-program-in-a-programming-contest-environment
*/

using namespace std;

static pid_t pid;

// This function kills the child program if it is called.
void alarm_handler(int sig) {
    cerr << "TLE";
    if (kill(pid, SIGKILL) != 0) {
        cerr << "TLE";
        perror("TLE kill");
    }
}

// This function checks for the stack and heap memory usage of a process,
// given its pid (process_id) and the procfile.  The memory usage
// is returned in kB.
int get_memory_usage(pid_t pid) {
	ifstream fs;
	string str = "/proc/" + to_string(pid) + "/status";
	fs.open(str);
    int data, stack;
    char buf[4096];
    char *vm;
 
    fs.read(buf, 4095);
    buf[4095] = '\0';
 
    data = stack = 0;
 
    vm = strstr(buf, "VmData:");
    if (vm) {
        sscanf(vm, "%*s %d", &data);
    }
    vm = strstr(buf, "VmStk:");
    if (vm) {
        sscanf(vm, "%*s %d", &stack);
    }
 
 	fs.close();
    return data + stack;
}

int main(int argc, char *argv[]) {
	int c;
    int memory_limit = 0;
    int time_limit = 0;
    string program;
    char *mopt = 0, *topt = 0, *popt = 0, *dopt = 0, *lopt = 0;
    static struct option long_options[] = {
        {"memory_limit", required_argument, 0, 'm'},
        {"time_limit",   required_argument, 0, 't'},
        {"program",      required_argument, 0, 'p'},
        {"directory",    required_argument, 0, 'd'},
        {"langauge",     required_argument, 0, 'l'}
    };
    int option_index = 0;
    while ( (c = getopt_long(argc, argv, "m:t:p:d:l:",
    	long_options, &option_index)) != -1) {
        switch (c) {
        case 'm':
            mopt = optarg;
            memory_limit = atoi(mopt);
            break;
        case 't':
            topt = optarg;
            time_limit = atoi(topt);
            break;
        case 'p':
            popt = optarg;
            break;
        case 'd':
            dopt = optarg;
            break;
        case 'l':
            lopt = optarg;
            break;
        case '?':
            break;
        default:
            printf ("?? getopt returned character code 0%o ??\n", c);
        }
    }

    // install an alarm handler for SIGALRM
    signal(SIGALRM, alarm_handler);
    // install an alarm to be fired after TIME_LIMIT
    alarm(time_limit);

	int status;

    if ((pid = fork()) == -1) { // error forking
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { // child
        // move to appropriate directory
	    if (chdir(dopt) != 0) {
            perror("chdir");
            return 1;
        }
        // create chroot jail in that directory
    	/*if (chroot(dopt) != 0) {
        	perror("chroot");
        	return 1;
    	}*/
        // Switch to non root user
        seteuid(65534);
        // execv child process in chroot jail
        string str(lopt);
        if (str != "python") {
            if (execv(popt, argv) == -1) {
                perror("execv");
                return 1;
            }
        } else if (str == "python") {
            if (execlp("python3", "python3", popt, (char *)NULL) == -1) {
                perror("execlp");
                return 1;
            }
        }
	} else {  // parent
        struct rusage resource_usage;
        // set arbitrary lower limit value of memory used
        int memory_used = 128;
        pid_t pid2;

        do {
            memory_used = max(memory_used, get_memory_usage(pid));
            if (memory_used > memory_limit) {
            	cerr << "MLE";
                if (kill(pid, SIGKILL) != 0) {
                    perror("MLE kill");
                }
                pid2 = wait4(pid, &status, WUNTRACED | WCONTINUED, &resource_usage);
            }
            else {
                pid2 = waitpid(pid, &status, WNOHANG);
                if (pid2 == 0)
	               sleep(1);
           }
        } while (pid2 == 0);
	}
	
	return 0;
}