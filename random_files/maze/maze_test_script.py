from maze_tester import *

m = MazeTester()

print "Should be True"
print (m.valid_maze("./maze-output/maze-5-15.txt"))
print (m.valid_maze("./maze-output/maze-8-4.txt"))
print (m.valid_maze("./maze-output/maze-30-40.txt"))

print " "
print "Should be False"
print (m.valid_maze("./maze-output/maze-5-15-cycles.txt"))
print (m.valid_maze("./maze-output/maze-5-15-unreachable.txt"))
print (m.valid_maze("./maze-output/maze-5-15-unsolvable.txt"))

print (m.valid_maze("./maze-output/maze-8-4-cycle.txt"))
print (m.valid_maze("./maze-output/maze-8-4-unreachable.txt"))
print (m.valid_maze("./maze-output/maze-8-4-unsolvable.txt"))