class MazeTester:
	def __init__(self):
		self.loop = False

	def valid_maze(self, in_file):
		'''
		Maze is a dictionary that represents each grid coordinate as a (row, col)
		tuple mapped to an adjacency list of (row, col) neighbors. End is a tuple
		representing the end of the maze.  Return True if a path exists and False
		otherwise.
		'''
		error = "No error"
		self.loop = False
		start = "0,0"
		(neighbors, end, size) = self.parse_maze(in_file)
		visited = set()
		result = self.dfs("-1,-1", start, end, neighbors, visited)

		if not result:
			error = "maze is unsolvable"
		elif result and len(visited) != size:
			error = "maze has unreachable portions"
			result = False
		elif result and self.loop:
			error = "maze contains one or more cycles"
			result = False

		return (in_file, result, error)

	def dfs(self, prev, start, end, neighbors, visited):
		if start == end:
			visited.add(start)
			return True
		if start in visited:
			self.loop = True
			return False

		visited.add(start)
		result = False
		for neighbor in neighbors[start]:
			if neighbor != prev:
				if self.dfs(start, neighbor, end, neighbors, visited) == True:
					result = True

		return result

	def get_neighbors(self, row, col, i, j, board):
		left = False
		if (j - 2) >= 0:
			left = board[i][j - 2] != '|'
		right = False
		if (j + 2) < len(board[0]):
			right = board[i][j + 2] != '|'
		up = False
		if (i + 1) < len(board):
			up = board[i - 1][j] != '-'
		down = False
		if (i - 1) >= 0:
			down = board[i + 1][j] != '-'

		result = []

		if left:
			result.append(str(row) + "," + str(col - 1))
		if right:
			result.append(str(row) + "," + str(col + 1))
		if up:
			result.append(str(row - 1) + "," + str(col))
		if down:
			result.append(str(row + 1) + "," + str(col))

		return result

	def parse_maze(self, in_file):
		'''
		Take infile and produce a dictionary representing each position and its
		reachable neighbors in the maze.
		'''
		result = {}
		with open(in_file) as f:
			board = f.readlines()

		for i in range(len(board)):
			board[i] = board[i].rstrip()

		end = board[0].split()
		size = int(end[0]) * int(end[1])
		end = str(int(end[0]) - 1) + "," + str(int(end[1]) - 1)
		board = board[1:]

		row = 0
		for i in range(1, len(board), 2):
			col = 0
			for j in range(2, len(board[i]), 4):
				result[str(row) + "," + str(col)] = self.get_neighbors(row, col, i, j, board)
				col += 1
			row += 1

		return (result, end, size)
