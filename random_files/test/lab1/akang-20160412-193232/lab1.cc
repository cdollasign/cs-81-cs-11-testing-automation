/*
 * Given user input of three points, calculates area of triangle formed by
 * the three argument points.
 */

#include <cmath>
#include <iostream>
#include "Point.hh"

using namespace std;


/*
 * Calculates area of triangle formed by the three argument points.
 */
double computeArea(Point &a, Point &b, Point &c)
{
  // Triangle sides
  double l1 = a.distanceTo(b);
  double l2 = b.distanceTo(c);
  double l3 = c.distanceTo(a);
  // Semiperimeter
  double s = (l1 + l2 + l3) / 2;

  // Finds area using Heron's formula.
  return sqrt(s * (s - l1) * (s - l2) * (s - l3));
}

int main()
{
  double x, y, z;

  // Gathers three points from user input.
  cout << "Point 1: ";
  cin >> x >> y >> z;
  Point p1(x, y, z);

  cout << "Point 2: ";
  cin >> x >> y >> z;
  Point p2(x, y, z);

  cout << "Point 3: ";
  cin >> x >> y >> z;
  Point p3(x, y, z);

  // Computes and prints area.
  cout << "Area is: " << computeArea(p1, p2, p3) << endl;
}
