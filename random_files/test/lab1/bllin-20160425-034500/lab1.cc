#include <iostream>
#include <cmath>
#include "Point.hh"
using namespace std;

// Computes the area of 3 3D points using Heron's formula
double computeArea(Point &x, Point &y, Point &z) {
	double a = x.distanceTo(y);
	double b = y.distanceTo(z);
	double c = z.distanceTo(x);
	double s = (a + b + c) / 2;
	return sqrt(s * (s - a) * (s - b) * (s - c));
}

//main function, user inputs 9 numbers in the console and is returned area.
int main() {
	cout << "Enter 9 numbers, representing x, y, and z coordinates." << endl;
	double x, y, z;
	Point a, b, c;
	for (int i = 0; i < 3; i++) {
		cout << "Enter x coordinate: " << endl;
		cin >> x;
		cout << "Enter y coordinate: " << endl;
		cin >> y;
		cout << "Enter z coordinate: " << endl;
		cin >> z;
		if (i == 0) {	
			a = Point(x, y, z);
		}
		else if (i == 1) {
			b = Point(x, y, z);
		}
		else if (i == 2) {
			c = Point(x, y, z);
		}
	}
	cout << "Point 1:  <" << a.getX() << " " << a.getY() << " " << a.getZ() << ">" << endl;
	cout << "Point 2:  <" << b.getX() << " " << b.getY() << " " << b.getZ() << ">" << endl;
	cout << "Point 3:  <" << c.getX() << " " << c.getY() << " " << c.getZ() << ">" << endl;
	cout << "Area is:  <" << computeArea(a,b,c) << ">" << endl;
}