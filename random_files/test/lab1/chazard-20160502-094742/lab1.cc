#include "Point.hh" //only include header files not the cc fie with the
                    //definitions or else it gets a multiple definition error
#include <iostream>
#include <math.h>
using namespace std;

//computes the area of the triangle formed by the passed in points
//using Heron's formula
double computeArea(Point& a, Point& b, Point& c)
{
  //s is midpoint distance in heron's formula
  double s=(a.distanceTo(b) + b.distanceTo(c) + c.distanceTo(a)) / 2.0;
  //* means dereferencing the pointer
  return sqrt(s * (s - a.distanceTo(b)) * (s - a.distanceTo(c)) * (s - b.distanceTo(c)));
}

int main()
{
  //unlike java you don't *need* the keyword new to instantiate a new object---you use new when instantiating
  //an object stored in the heap and that returns a pointer (pointers are easier to use in c++ but you have to
  //remember to delete them or else you get a memory leak), otherwise if you declare it without the new keyword
  //you are storing the object itself (not the pointer) and you can therefore dereference it with & to get its
  //memory location---when doing it this way you need the & in the function arguments above otherwise it wastes
  //time making a local copy of the object just for that function
  //by the way, if you are not using new to instantiate the object (and therefore not using the pointer) you don't
  //need to delete anything since it is on the stack and will therefore automatically garbage collect--only have
  //to explicitly free things on the heap
  Point points [3];
  double x,y,z;
  int i;
  while(true){
    for(i = 0;i < 3;i++)
      {
	cout << "Enter x coordinate for point "<<(i + 1)<<": ";
	cin >> x;
	cout << "Enter y coordinate for point "<<(i + 1)<<": ";
	cin >> y;
	cout << "Enter z coordinate for point "<<(i + 1)<<": ";
	cin >> z;
	//see note above
	points[i]= Point(x,y,z);
      }
    cout << "Point 1: <"<< points[0].getX() << " " << points[0].getY()<< " " << points[0].getZ() <<">\n";
    cout << "Point 2: <"<< points[1].getX() << " " << points[1].getY()<< " " << points[1].getZ() <<">\n";
    cout << "Point 3: <"<< points[2].getX() << " " << points[2].getY()<< " " << points[2].getZ() <<">\n";
    cout << "Area is: <"<< computeArea(points[0], points[1], points[2]) <<">\n";
    cout << "Enter another one?(Y or N)";
    char s;
    cin >> s;
    if(s != 'Y' && s != 'y')
      break;
  }
  return 0;
}
//to compile: g++ -std=c++14 -Wall lab1.cc Point.cc -o lab1
//specifies GNU c++ compiler, c++ version 2014, -Wall outputs errors and warnings
//the list the files (headers not included since they are automatically pulled)
//then the name of the compiled program (note that you can only have one main in
//all of the cc programs you mention and that is what the executable runs)

//then to run the executable do ./lab1
