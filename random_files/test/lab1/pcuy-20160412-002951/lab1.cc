#include <cmath>
#include <iostream>
#include "Point.hh"

using namespace std;

/* Computes the area within the triangle defined by three points using 
   Heron's formula. */
double computeArea(Point &a, Point &b, Point &c) {
    // Calculate the side lengths of the triangle.
    double side1 = a.distanceTo(b);
    double side2 = b.distanceTo(c);
    double side3 = c.distanceTo(a);

    // Compute the semiperimeter of the triangle.
    double s = 0.5 * (side1 + side2 + side3);

    // Use Heron's formula to get triangle area.
    return sqrt(s * (s - side1) * (s - side2) * (s - side3));
}

int main() {
    double x, y, z;

    // Prompt the user to provide the x, y, z coordinates for 3 points.
    cout << "Point 1: ";
    cin >> x >> y >> z;
    Point a(x, y, z);
    cout << "Point 2: ";
    cin >> x >> y >> z;
    Point b(x, y, z);
    cout << "Point 3: ";
    cin >> x >> y >> z;
    Point c(x, y, z);

    // Calculate the area within the triangle defined by the three points.
    cout << "Area is: " <<  computeArea(a, b, c) << endl;
}