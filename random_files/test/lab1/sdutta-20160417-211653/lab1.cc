#include <cmath>
#include <iostream>
#include "Point.hh"
using namespace std;

// Given three 3d Point objects, this method calculates the area of 
// the triangle enclosed
double computeArea(Point &a, Point &b, Point &c) {
  double len1 = a.distanceTo(b);
  double len2 = b.distanceTo(c);
  double len3 = c.distanceTo(a);
  double s = (len1 + len2 + len3) / 2; // semiperimeter
  double area = sqrt(s * (s - len1) * (s - len2) * (s - len3));
  return area;
}

// Asks for user to input three 3-coordinate points, outputs
// the area of the triangle enclosed.
int main() {
  double x, y, z;
  cout << "Point 1: ";
  cin >> x >> y >> z;
  Point a(x, y, z);

  cout << "Point 2: ";
  cin >> x >> y >> z;
  Point b(x, y, z);

  cout << "Point 3: ";
  cin >> x >> y >> z;
  Point c(x, y, z);

  double area = computeArea(a, b, c);
  cout << "Area is: " << area << endl;

  return 0;
}