#include <iostream>
#include <cmath>
#include "Point.hh"
using namespace std;

// Calculate the area of a triangle with points at a, b, and c, using 
// an alternate form of Heron's formula.
double computeArea(Point &a, Point &b, Point &c) {
  double s1 = a.distanceTo(b);
  double s2 = b.distanceTo(c);
  double s3 = c.distanceTo(a);
  double n = s1 * s1 + s2 * s2 - s3 * s3;
  return 0.25 * sqrt(4 * s1 * s1 * s2 * s2 - n * n);
}

int main() {
  // Handle input:
  double x, y, z;
  cout << "Input an x, y, and z coordinate for each point." << endl;
  cin >> x >> y >> z;
  Point a(x, y, z);
  cin >> x >> y >> z;
  Point b(x, y, z);
  cin >> x >> y >> z;
  Point c(x, y, z);

  // Calculate area:
  double area = computeArea(a, b, c);

  // Do output:
  cout << "Point 1:  <" << a.getX() << " " << a.getY() << " " 
    << a.getZ() << ">" << endl << "Point 2:  <" << b.getX() << " " 
    << b.getY() << " " << b.getZ() << ">" << endl << "Point 3:  <" 
    << c.getX() << " " << c.getY() << " " << c.getZ() << ">" << endl 
    << "Area is: <" << area << ">" << endl;
}
