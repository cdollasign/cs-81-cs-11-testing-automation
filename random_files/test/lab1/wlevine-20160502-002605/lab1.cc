// lab1.cc
// Garrett Levine, CS11 C++
// April 11, 2016

#include <iostream>
#include "Point.hh"
#include <cmath>
using namespace std;

double computeArea(Point &a, Point &b, Point &c) {
    // Takes three inputs of the Point class and
    // computes the area of the triangle that these make.
    double sA = a.distanceTo(b);
    double sB = a.distanceTo(c);
    double sC = b.distanceTo(c);
    double s = ((sA + sB + sC)/2.0);
    //cout << sA  << " " << sB << " " << sC << " " << s << endl;
    return sqrt(s * (s - sA) * (s - sB) * (s - sC));
}

int main() {
    double x1, y1, z1;
    cout << "Enter the points!" << endl;
    cin >> x1 >> y1 >> z1;
    Point pointa(x1, y1, z1);
    cout << "Point 1:   <" << x1 << " " << y1 << " " << z1 << ">" << endl;
    cin >> x1 >> y1 >> z1;
    Point pointb(x1, y1, z1);
    cout << "Point 2:   <" << x1 << " " << y1 << " " << z1 << ">" << endl;
    cin >> x1 >> y1 >> z1;
    Point pointc(x1, y1, z1);
    cout << "Point 3:   <" << x1 << " " << y1 << " " << z1 << ">" << endl;
    cout << "Area is:   " << "<" << computeArea(pointa, pointb, pointc) << ">" << endl;
    return 0;
}