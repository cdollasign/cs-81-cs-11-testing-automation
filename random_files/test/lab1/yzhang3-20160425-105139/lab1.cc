#include "Point.hh"
#include <iostream>
using namespace std;

/*
 * This function will compute the area of the triangle which consists of
 * three points 'a', 'b' and 'c'/
 */
double computeArea(Point &a, Point &b, Point &c)
{
    double d_ab, d_bc, d_ca, s;
    d_ab = a.distanceTo(b);
    d_bc = b.distanceTo(c);
    d_ca = c.distanceTo(a);
    s = (d_ab + d_bc + d_ca) / 2.0;
    return sqrt(s * (s - d_ab) * (s - d_bc) * (s - d_ca));
}

/*
 * This function prints the points of a triangle and its area.
 */
void print(Point &p1, Point &p2, Point &p3)
{
	cout << endl;
    cout << "Point 1: (" << p1.getX() << ", " << p1.getY() << ", " 
    << p1.getZ() << ")" << endl;
    cout << "Point 2: (" << p2.getX() << ", " << p2.getY() << ", " 
    << p2.getZ() << ")" << endl;
    cout << "Point 3: (" << p3.getX() << ", " << p3.getY() << ", " 
    << p3.getZ() << ")" << endl;
    cout << "Area is " << computeArea(p1, p2, p3) << endl;
}

/*
 * Tests the class 'Point' and the function 'computeArea'.
 */
int main()
{
	double x, y, z;
    cout << "Please enter the x, y, z coordinates for Point 1: ";
    cin >> x >> y >> z;
    Point p1(x, y, z);
    
    cout << "Please enter the x, y, z coordinates for Point 2: ";
    cin >> x >> y >> z;
    Point p2(x, y, z);
    
    cout << "Please enter the x, y, z coordinates for Point 3: ";
    cin >> x >> y >> z;
    Point p3(x, y, z);

    print(p1, p2, p3);


    return 0;
}

